#include <SFML/Graphics.hpp>

#include <string>

int main()
{
    const int SCREEN_WIDTH = 1280;
    const int SCREEN_HEIGHT = 720;
    const std::string SCREEN_TITLE = "My Game";

    sf::RenderWindow window( sf::VideoMode( SCREEN_WIDTH, SCREEN_HEIGHT ), SCREEN_TITLE );

    while ( window.isOpen() )
    {
        // GET INPUT
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // DRAW
        window.clear( sf::Color::Blue );
        window.display();
    }

    return 0;
}
